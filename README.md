# qdbus-qt5

qdbus split from qt5-tools package for Archlinux

Some programs written in qt expect qdbus binary to be present on the system to function. One example is KDE's screenshot tool, Spectacle.
Since Arch isn't big on splitting packages, qdbus binary comes bundled in a qt5-tools package, which is an overkill and likely installs 
software nobody asked for/cares for.

This PKGBUILD will take the qdbus binary directly from the official qt5-tools package and repackage it. It 'provides' qt5-tools and as 
such it can act as a replacement for the heavyweight qt5-tools for the purposes of programs which rely only on qdbus. This version takes
up only 49.83kb disk space.

Please note that the mirror in the source array is hardcoded, I haven't found a way to specify alternatives/mirrors. Feel free to change
the server URL to a more suitable mirror.

Additional note: pkgver and pkgrel need to be kept in sync with current version of qt5-tools.

TODO:
Write a prepare function that does this automatically for us.
